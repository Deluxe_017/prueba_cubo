using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravedadPlaneta : MonoBehaviour
{
    public GameObject gravedad_Cubo;

    public float velocidad;
    public float velocidadRotacion;

    void Update()
    {
        Physics.gravity = gravedad_Cubo.transform.position - transform.position;

        transform.rotation = Quaternion.FromToRotation(transform.up, -Physics.gravity) * transform.rotation;

        if (Input.GetKey(KeyCode.W)) 
        {
            transform.Translate(new Vector3(0,0, velocidad * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.S)) 
        {
            transform.Translate(new Vector3(0,0, -velocidad * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.A)) 
        {
            transform.Rotate(new Vector3(0, -velocidadRotacion * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.D)) 
        {
            transform.Rotate(new Vector3(0, velocidadRotacion * Time.deltaTime, 0));
        }
    }
}